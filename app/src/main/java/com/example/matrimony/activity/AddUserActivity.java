package com.example.matrimony.activity;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.example.matrimony.R;
import com.example.matrimony.adapter.CityAdapter;
import com.example.matrimony.adapter.LanguageAdapter;
import com.example.matrimony.database.TblMstCity;
import com.example.matrimony.database.TblMstLanguage;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.model.CityModel;
import com.example.matrimony.model.LanguageModel;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Constant;
import com.example.matrimony.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

;

public class AddUserActivity<view> extends BaseActivity {

    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etFatherName)
    TextInputEditText etFatherName;
    @BindView(R.id.etSurname)
    TextInputEditText etSurname;
    @BindView(R.id.rbFemale)
    MaterialRadioButton rbFemale;
    @BindView(R.id.rbMale)
    MaterialRadioButton rbMale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    TextInputEditText etDob;
    @BindView(R.id.etPhoneNumber)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    TextInputEditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.etLanguage)
    TextView etLanguage;
    @BindView(R.id.etCity)
    TextView etCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.cbActCricket)
    CheckBox cbActCricket;
    @BindView(R.id.cbActBadminton)
    CheckBox cbActBadminton;
    @BindView(R.id.cbActHockey)
    CheckBox cbActHockey;
    @BindView(R.id.cbActMusic)
    CheckBox cbActMusic;
    @BindView(R.id.cbActDancing)
    CheckBox cbActDancing;
    @BindView(R.id.cbActOthers)
    CheckBox cbActOthers;
    String startingDate = "1990-08-10";

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();
    @BindView(R.id.screen_layout)
    TableLayout screenLayout;
    UserModel userModel;

    @BindView(R.id.background)
    LinearLayout background;


    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_add_user), true);
        setDataToView();
        getUserOnUpdate();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Sriracha-Regular.ttf");
        etName.setTypeface(typeface);
        etFatherName.setTypeface(typeface);
        etSurname.setTypeface(typeface);
        etEmailAddress.setTypeface(typeface);
        etPhoneNumber.setTypeface(typeface);
        btnSubmit.setTypeface(typeface);
        rbMale.setTypeface(typeface);
        rbFemale.setTypeface(typeface);
        cbActOthers.setTypeface(typeface);
        cbActCricket.setTypeface(typeface);
        cbActDancing.setTypeface(typeface);
        cbActMusic.setTypeface(typeface);
        cbActHockey.setTypeface(typeface);
        cbActBadminton.setTypeface(typeface);
        etDob.setTypeface(typeface);
        etCity.setTypeface(typeface);
        etLanguage.setTypeface(typeface);
    }

    void getUserOnUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.lbl_edit_user);

            etName.setText(userModel.getName());
            etFatherName.setText(userModel.getFatherName());
            etSurname.setText(userModel.getSurname());
            etEmailAddress.setText(userModel.getEmail());
            etPhoneNumber.setText(userModel.getPhoneNumber());
            etDob.setText(userModel.getDob());
            if (userModel.getGender() == Constant.MALE) {
                rbMale.setChecked(true);
            } else {
                rbFemale.setChecked(true);
            }
            spCity.setSelection(getSelectedPositionFromCityID(userModel.getCityID()));
            spLanguage.setSelection(getSelectedPositionFromLanguageID(userModel.getLanguageID()));
        }
    }

    int getSelectedPositionFromCityID(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityID() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageID(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getLanguageID() == languageId) {
                return i;
            }
        }
        return 0;
    }

    void setSpinnerAdapter() {
        cityList.addAll(new TblMstCity(this).getCityList());
        languageList.addAll(new TblMstLanguage(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);
    }

    void setDataToView() {
        final Calendar newCalender = Calendar.getInstance();
        etDob.setText(
                String.format("%02d", newCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                        String.format("%02d", newCalender.get(Calendar.MONTH) + 1) + "/" +
                        String.format("%02d", newCalender.get(Calendar.YEAR)));
        setSpinnerAdapter();
    }

    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddUserActivity.this,
                (DatePicker datePicker, int year, int month, int day) -> {
                    etDob.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year);
                }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
        if (isValidUser()) {
            if (userModel == null) {
                long lastInsertedID = new TblUser(getApplicationContext()).insertUser(etName.getText().toString(),
                        etFatherName.getText().toString(), etSurname.getText().toString(),
                        etEmailAddress.getText().toString(),
                        rbMale.isChecked() ? Constant.MALE : Constant.FEMALE, "",
                        Utils.getFormatedDate(etDob.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityID(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(), 0);
                showToast(lastInsertedID > 0 ? "User Inserted Successfully" : "Something went wrong");
            } else {
                long lastInsertedID = new TblUser(getApplicationContext()).updateUserById(etName.getText().toString(),
                        etFatherName.getText().toString(), etSurname.getText().toString(),
                        etEmailAddress.getText().toString(),
                        rbMale.isChecked() ? Constant.MALE : Constant.FEMALE, "",
                        Utils.getFormatedDate(etDob.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityID(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(), userModel.getIsFavorite(), userModel.getUserId());
                showToast(lastInsertedID > 0 ? "User Updated Successfully" : "Something went wrong");
            }

        }
    }


    boolean isValidUser() {
        boolean isvalid = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            isvalid = false;
            etName.setError(getString(R.string.error_enter_name));
        }
        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isvalid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }
        if (TextUtils.isEmpty(etSurname.getText().toString())) {
            isvalid = false;
            etSurname.setError(getString(R.string.error_surname));
        }
        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isvalid = false;
            etPhoneNumber.setError(getString(R.string.error_number));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isvalid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_number));
        }
        if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
            isvalid = false;
            etEmailAddress.setError(getString(R.string.error_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString()).matches()) {
            isvalid = false;
            etEmailAddress.setError(getString(R.string.error_valid_email));
        }

        if (spCity.getSelectedItemPosition() == 0) {
            isvalid = false;
            showToast(getString(R.string.error_city));
        }
        if (spLanguage.getSelectedItemPosition() == 0) {
            isvalid = false;
            showToast(getString(R.string.error_language));
        }

        if (!(cbActCricket.isChecked() || cbActBadminton.isChecked() || cbActHockey.isChecked() || cbActMusic.isChecked() ||
                cbActDancing.isChecked() || cbActOthers.isChecked())) {
            Toast.makeText(this, "Please select any one checkbox", Toast.LENGTH_LONG).show();
            isvalid = false;
            showToast(getString(R.string.error_checkbox));
        }
        return isvalid;

    }
}
