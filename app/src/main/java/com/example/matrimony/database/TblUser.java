package com.example.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Surface;
import android.view.animation.AnticipateOvershootInterpolator;

import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Utils;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SURNAME = "Surname";
    public static final String EMAIL = "Email";
    public static final String GENDER = "Gender";
    public static final String HOBBIES = "Hobbies";
    public static final String DOB = "Dob";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String CITY_ID = "CityID";
    public static final String IS_FAVORITE = "IsFavorite";

    /* QUERY COLUMN */
    public static final String CITY = "City";
    public static final String LANGUAGE = "Language";
    public static final String AGE = "Age";


    public TblUser(Context context) {
        super(context);
    }


    public ArrayList<UserModel> getUserList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT " +
                " UserId," +
                " TblUser.Name as Name," +
                " FatherName," +
                " Surname," +
                " Gender," +
                " Email," +
                " IsFavorite," +
                " Dob," +
                " PhoneNumber," +
                " TblMstLanguage.LanguageID," +
                " TblMstCity.CityID," +
                " TblMstLanguage.Name as Language," +
                " TblMstCity.Name as City " +

                "FROM " +
                " TblUser " +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                " INNER JOIN TblMstCity ON TblUser.CityID = TblMstCity.CityID";

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount() ;i++) {
            UserModel model = new UserModel();
            model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
            model.setSurname(cursor.getString(cursor.getColumnIndex(SURNAME)));
            model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
//            model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
            model.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DOB))));
            model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            model.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            model.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            model.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
            model.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
            model.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
            list.add(model);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getUserById(int id){
        SQLiteDatabase db = getReadableDatabase();
        UserModel model = new UserModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ID + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        model.setSurname(cursor.getString(cursor.getColumnIndex(SURNAME)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
//        model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        model.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
        model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        model.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        model.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
        cursor.close();
        db.close();
        return model;
    }

    public ArrayList<UserModel> getUserListByGender(int gender){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT " +
                " UserId," +
                " TblUser.Name as Name," +
                " FatherName," +
                " Surname," +
                " Gender," +
                " Email," +
                " IsFavorite," +
                " Dob," +
                " PhoneNumber," +
                " TblMstLanguage.LanguageID," +
                " TblMstCity.CityID," +
                " TblMstLanguage.Name as Language," +
                " TblMstCity.Name as City " +

                "FROM " +
                " TblUser " +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                " INNER JOIN TblMstCity ON TblUser.CityID = TblMstCity.CityID" +

                " WHERE " +
                " Gender = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        Log.d("userList::", "" +cursor.getCount());
        for (int i=0; i < cursor.getCount(); i++) {
            UserModel model = new UserModel();
            model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
            model.setSurname(cursor.getString(cursor.getColumnIndex(SURNAME)));
            model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
//            model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
            model.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DOB))));
            model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            model.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            model.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            model.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
            model.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
            model.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
            list.add(model);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<UserModel> getFavoriteUserList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT " +
                " UserId," +
                " TblUser.Name as Name," +
                " FatherName," +
                " Surname," +
                " Gender," +
                " Email," +
                " IsFavorite," +
                " Dob," +
                " PhoneNumber," +
                " TblMstLanguage.LanguageID," +
                " TblMstCity.CityID," +
                " TblMstLanguage.Name as Language," +
                " TblMstCity.Name as City " +

                "FROM " +
                " TblUser " +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                " INNER JOIN TblMstCity ON TblUser.CityID = TblMstCity.CityID" +

                " WHERE " +
                " IsFavorite = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            UserModel model = new UserModel();
            model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
            model.setSurname(cursor.getString(cursor.getColumnIndex(SURNAME)));
            model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
//            model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
            model.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DOB))));
            model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            model.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            model.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            model.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
            model.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
            model.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
            list.add(model);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public long insertUser(String name, String fatherName, String surname, String email, int
            gender, String hobbies, String date, String phoneNumber, int languageID, int cityID, int isFavorite){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SURNAME, surname);
        cv.put(EMAIL, email);
        cv.put(GENDER, gender);
        cv.put(HOBBIES, hobbies);
        cv.put(DOB, date);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(LANGUAGE_ID, languageID);
        cv.put(CITY_ID, cityID);
        cv.put(IS_FAVORITE, isFavorite);
        long lastInsertedID = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedID;
    }


    public int updateUserById(String name, String fatherName, String surname, String email,
                           int gender, String hobbies, String date, String phoneNumber,
                           int languageID, int cityID, int isFavorite, int userId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SURNAME, surname);
        cv.put(EMAIL, email);
        cv.put(GENDER, gender);
//        cv.put(HOBBIES, hobbies);
        cv.put(DOB, date);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(LANGUAGE_ID, languageID);
        cv.put(CITY_ID, cityID);
        cv.put(IS_FAVORITE, isFavorite);
        int lastUpdatedId = db.update(TABLE_NAME,cv,USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;
    }

    public int deleteUserById(int userId){
        SQLiteDatabase db = getWritableDatabase();
        int deleteUserId = db.delete(TABLE_NAME, USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return deleteUserId;
    }

    public int updateFavoriteStatus(int isFavorite, int userId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IS_FAVORITE, isFavorite);
        int lastUpdatedId = db.update(TABLE_NAME,cv,USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;
    }
}
